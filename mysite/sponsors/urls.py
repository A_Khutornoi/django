from django.conf.urls import patterns, url
from sponsors import views

urlpatterns = patterns('',
url(r'^$', views.index, name='index'),
url(r'^new/sponsor/$', views.sponsor_new, name ="sponsor_new"),
url(r'^(?P<pk>\d+)/edit/$', views.sponsor_edit, name='sponsor_edit'),
url(r'^(?P<pk>\d+)/delete/$', views.sponsor_delete, name='delete'),
url(r'^(?P<pk>\d+)$', views.about, name='about'),
)