from django.db import models

class Sponsor(models.Model):
	name = models.CharField(max_length=255)
	description = models.TextField(max_length=255)
	logo = models.ImageField(null = True, blank = True)
	
			
	def __unicode__(self):
		return self.name

	def __str__(self):
		return self.name
	