from django.contrib import admin

from .models import Sponsor

class SponsorAdmin(admin.ModelAdmin):
	search_fields = ['name']

admin.site.register(Sponsor,SponsorAdmin)