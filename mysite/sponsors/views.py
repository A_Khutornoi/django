from django.shortcuts import render, redirect,  get_object_or_404
from .forms import SponsorForm
from .models import Sponsor
import datetime
from performances.models import Performance
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger



def index(request):
	sponsors_list = Sponsor.objects.all()

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)

	paginator = Paginator(sponsors_list, 5)
	page = request.GET.get('page')
	try:
		contacts = paginator.page(page)
	except PageNotAnInteger:
	# If page is not an integer, deliver first page.
		contacts = paginator.page(1)
	except EmptyPage:
	# If page is out of range (e.g. 9999), deliver last page of results.
		contacts = paginator.page(paginator.num_pages)
	context = {'Sponsor':contacts,'now':now, 'perf':perf[:5]}
	return render(request, 'sponsors.html', context) 

def sponsor_new(request):
	
	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	if request.method == "POST":
		form = SponsorForm(request.POST,request.FILES)


		if form.is_valid():
			Sponsor = form.save(commit ="False")
			Sponsor.save()	
			return redirect('sponsors:index')
	else:
		form = SponsorForm()
	return render(request,'sponsor_edit.html', {'form':form,'now':now, 'perf':perf[:5]})

def sponsor_edit(request, pk):
	sponsor = get_object_or_404(Sponsor, pk = pk)

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	if request.method == "POST":
		form = SponsorForm(request.POST,request.FILES, instance = sponsor)
		if form.is_valid():
			sponsor = form.save(commit = False)
			sponsor.save()
			return redirect('sponsors:index')
	else:
		form = SponsorForm(instance = sponsor)

	return render(request, "sponsor_edit.html",{"form":form,'now':now, 'perf':perf[:5]})

def sponsor_delete(request, pk):
	sponsor = get_object_or_404(Sponsor, pk = pk)
	sponsor.delete()
	return redirect('sponsors:index')


def about(request, pk):
	sponsor = get_object_or_404(Sponsor, pk = pk)

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	context = {'sponsor':sponsor,'now':now, 'perf':perf[:5]}
	return render(request, 'about_sponsor.html', context)