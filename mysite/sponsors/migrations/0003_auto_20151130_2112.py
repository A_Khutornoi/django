# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sponsors', '0002_sponsors_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sponsors',
            name='logo',
            field=models.ImageField(upload_to=''),
        ),
    ]
