# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sponsors', '0004_auto_20151209_0031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sponsor',
            name='description',
            field=models.TextField(max_length=255),
        ),
        migrations.AlterField(
            model_name='sponsor',
            name='logo',
            field=models.ImageField(upload_to='', null=True, blank=True),
        ),
    ]
