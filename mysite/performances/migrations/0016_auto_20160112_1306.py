# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performances', '0015_auto_20160101_1450'),
    ]

    operations = [
        migrations.AlterField(
            model_name='performance',
            name='pub_date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
