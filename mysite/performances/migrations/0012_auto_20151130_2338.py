# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performances', '0011_auto_20151130_2337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='performances',
            name='time',
            field=models.DateTimeField(),
        ),
    ]
