# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performances', '0012_auto_20151130_2338'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='performances',
            options={'ordering': ['time']},
        ),
        migrations.AlterField(
            model_name='performances',
            name='description',
            field=models.CharField(max_length=355),
        ),
        migrations.AlterField(
            model_name='performances',
            name='pub_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
