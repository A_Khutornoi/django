from django.conf.urls import patterns, url

from performances import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='performances'),
	url(r'^(?P<p>\d+)$', views.about, name='about'),
)