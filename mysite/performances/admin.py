from django.contrib import admin

from .models import Performance

class PerfAdmin(admin.ModelAdmin):
	search_fields = ['name']

admin.site.register(Performance,PerfAdmin)
