from django.db import models

class Performance(models.Model):
	name = models.CharField(max_length=55)
	img = models.ImageField(null = True, blank = True)
	description = models.TextField()
	time = models.DateTimeField()
	pub_date = models.DateTimeField(auto_now=True)

	class Meta:
		ordering = ['time']

	def __str__(self):
		return self.name

	def __unicode__(self):
		return self.name
