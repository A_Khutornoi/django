from django.db import models

class Main(models.Model):
	title = models.CharField(max_length=70)
	description = models.TextField()
	pub_date = models.DateTimeField(auto_now=True)
	
	class Meta:
		verbose_name = 'News'
		verbose_name_plural = 'News'
			
	
		
	def __unicode__(self):
		return self.title

	def __str__(self):
		return self.title
# Create your models here.
