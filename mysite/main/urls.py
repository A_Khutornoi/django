from django.conf.urls import patterns, url

from main import views

urlpatterns = patterns('',
	url(r'^news/(?P<pk>\d+)$', views.about, name='about'),
	url(r'^news/', views.index, name='news'),
	url(r'^$', views.default, name='default'),
)