"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.contrib.flatpages import views
import django.views.defaults


urlpatterns = [


    url(r'^', include('main.urls', namespace="main")),
    url(r'^speakers/', include('speakers.urls',  namespace="speakers")),
    url(r'^sponsors/', include('sponsors.urls', namespace="sponsors")),
    url(r'^performances/', include('performances.urls', namespace="performances")),
    url(r'^organizers/', include('organizers.urls', namespace="organizers")),
    url(r'^search/', include('search.urls')),
    url(r'^about-us/$', views.flatpage, {'url': '/about-us/'}, name='about'),
    url(r'^users/', include('users.urls')),
    url(r'^404/$', django.views.defaults.page_not_found),

        


    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
