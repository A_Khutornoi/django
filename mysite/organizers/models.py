from django.db import models

class Organizer(models.Model):
	fio = models.CharField(max_length=70)
	description = models.TextField()
	comunications = models.TextField()
	photo = models.ImageField()
	
	def __unicode__(self):
	        return self.fio

	def __str__(self):
	        return self.fio

