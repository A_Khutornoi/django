# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('fio', models.CharField(max_length=70)),
                ('description', models.CharField(max_length=255)),
                ('comunications', models.CharField(max_length=70)),
                ('photo', models.ImageField(upload_to='')),
            ],
        ),
    ]
