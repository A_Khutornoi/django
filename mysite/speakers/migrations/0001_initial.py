# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Speakers',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('fio', models.CharField(max_length=70)),
                ('description', models.CharField(max_length=255)),
                ('photo', models.ImageField(upload_to='')),
            ],
        ),
    ]
